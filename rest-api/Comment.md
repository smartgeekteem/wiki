[Главная](Home.md)
[Лайки](Like.md)
[Попутчики](TravelUser.md)



## Описание запросов к веб-сервису


## **Получение всех комментариев выбранного путешествия**

### GET http://domainname/api/post?travel_id={travel_id}
#### Выдаются последние 20 комментариев

Для получение следующих 20-ти комментариев необхожимо отправить запрос вида:
**GET http://domainname/api/post?travel_id={travel_id}&page={N}**

N - номер страницы с результатом поиска

travel_id: INT - идентификатор выбранного путешествия

Заголовки:

```
не требуется
```

Ответ сервера:

```
[
  {
    "date": 11.02.2017,
    "nickName": "nickName",
    "avatar": "/file/get/5sdf4sdsdfsf5s6df5s65dfs65f",
    "text": "это классный маршрут"
  },
…
  {
      "date": 15.02.2017,
      "nickName": "nickName",
      "avatar": "/file/get/5sdf4sdssdsdsd65dfs65f",
      "text": "я с вами"
    },
]
```




## **Получение одного комментария по его идентификатору**

### GET http://domainname/api/post/{post_id}

post_id : INT - идентификатор комментария


Заголовки: 

```
не требуется
```
Ответ сервера:

```
{
    "date": 11.02.2017,
    "nickName": "nickName",
    "avatar": "/file/get/5sdf4sdsdfsf5s6df5s65dfs65f",
    "text": "это классный маршрут"
  }
```


## **Изменение одного комментария по его идентификатору**

### PUT http://domainname/api/post/{post_id}

post_id : INT - идентификатор комментария


Заголовки: 

```
“Content-Type: application/json”
"Authorization: Bearer a7H9SZUn2a59CLUdDNgHuJQNB5BSggM0"
```

Тело запроса:

```
{
       "text": "это классный маршрут "
 }
```

Ответ сервера:

```
{
    "date": 11.02.2017,
    "nickName": "nickName",
    "avatar": "/file/get/5sdf4sdsdfsf5s6df5s65dfs65f",
    "text": "это классный маршрут"
  }
```

## **Добавление одного комментария**

### POST http://domainname/api/post

Заголовки: 

```
“Content-Type: application/json”
"Authorization: Bearer a7H9SZUn2a59CLUdDNgHuJQNB5BSggM0"
```

Тело запроса:

```
{
       "user_id": "2",
       "travel_id": "2",
       "text": "это классный маршрут "
 }
```

Ответ сервера:

```
{
       "id": "5",
       "user_id": "2",
       "travel_id": "2",
       "text": "это классный маршрут ",
       "lastmodified": "11.01.2017"
 }
```

## **Удаление одного комментария по идентификатору**

### DELETE http://domainname/api/post/{post_id}

post_id : INT - идентификатор комментария


Заголовки:

```
 "Authorization: Bearer a7H9SZUn2a59CLUdDNgHuJQNB5BSggM0"
```

Тело запроса:

```
не требуется
```

Ответ сервера:

Заголовоки ответа:
```
HTTP/1.1 204 No Content
```

Тело ответа:
```
Не требуется
```

[Главная](Home.md)
[Лайки](Like.md)
[Попутчики](TravelUser.md)
