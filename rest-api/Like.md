[Главная](Home.md)
[Комментарии](Comment.md)
[Попутчики](TravelUser.md)

## Описание запросов к веб-сервису


## **Получение количества(count) лайков выбранного путешествия**

### GET http://domainname/api/like?travel_id={travel_id}&count=true

Заголовки:

```
не требуется
```
Ответ сервера:

```
  {
      "count":"5"
  }

```


## **Добавление лайка для путешествия**

### POST http://domainname/api/like

Заголовки: 

```
“Content-Type: application/json”
"Authorization: Bearer a7H9SZUn2a59CLUdDNgHuJQNB5BSggM0"
```

Тело запроса:

```
{
    "user_id":"1"
    "travel_id": "3",
    "created": "20.02.2017",
  }
```

Ответ сервера:

```
{
    "id": "1",
    "travel_id": "123",
    "created": "20.02.2017",
  }
```

## **Удаление лайка**

### DELETE http://domainname/api/like/{like_id}

like_id : INT - идентификатор лайка


Заголовки:

```
"Authorization: Bearer a7H9SZUn2a59CLUdDNgHuJQNB5BSggM0"
```

Тело запроса:

```
{
   не требуется
}
```

Ответ сервера:

Заголовоки ответа:
```
HTTP/1.1 204 No Content
```

Тело ответа:
```
Не требуется
```

[Главная](Home.md)
[Комментарии](Comment.md)
[Попутчики](TravelUser.md)
