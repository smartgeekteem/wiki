[Главная](Home.md)
[Комментарии](Comment.md)
[Лайки](Like.md)

## Описание запросов к веб-сервису


## **Получение всех попутчиков для выбранного путешествия**

### GET http://domainname/api/travel-user?travel_id={travel_id}

travel_id : INT - идентификатор путешествия

Заголовки:

```
не требуется
```
Ответ сервера:

```
[
    {
        "travel_id":{travel_id},
        "user_id":"1"
    },
    ...
    {
        "travel_id":{travel_id},
        "user_id":"2"
    }
]
```


## **Присоединение пользователя к выбранному путешествию**

### POST http://domainname/api/travel-user

Заголовки: 

```
“Content-Type: application/json”
"Authorization: Bearer a7H9SZUn2a59CLUdDNgHuJQNB5BSggM0"
```

Тело запроса:

```
{
    "user_id":"1"
    "travel_id": "3",
  }
```

Ответ сервера:

Заголовок ответа:
```
HTTP/1.1 201 Created
Location: http://domainname/api/travel-user/3,1
```

Тело ответа:
```
{
    "user_id":"1",
    "travel_id":"3"
}
```

## **Отписать пользователя от выбранного путешествия**

### DELETE http://domainname/api/travel-user/{travel_id},{user_id}

travel_id: INT - идентификатор путешествия
user_id: INT - идентификатор пользователя


Заголовки:

```
"Authorization: Bearer a7H9SZUn2a59CLUdDNgHuJQNB5BSggM0"
```

Тело запроса:

```
{
   не требуется
}
```

Ответ сервера:

Заголовоки ответа:
```
HTTP/1.1 204 No Content
```

Тело ответа:
```
Не требуется
```

[Главная](Home.md)
[Комментарии](Comment.md)
[Лайки](Like.md)

